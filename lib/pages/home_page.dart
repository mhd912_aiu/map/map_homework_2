// home_page.dart
import 'package:flutter/material.dart';
import 'package:map_homework_2/main.dart';
import 'dart:math';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  var random = Random();
  late double _deviceWidth, _deviceHeight;
  late String _firstDiceImage, _secondDiceImage;
  int _firstDiceValue = 1, _secondDiceValue = 1;
  late ThemeProvider _themeProvider;

  @override
  Widget build(BuildContext context) {
    _firstDiceImage = 'assets/images/$_firstDiceValue.png';
    _secondDiceImage = 'assets/images/$_secondDiceValue.png';
    _deviceHeight = MediaQuery.of(context).size.height;
    _deviceWidth = MediaQuery.of(context).size.width;

    _themeProvider = Provider.of<ThemeProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: const Text("Game of Dice!"),
      ),
      drawer: _buildDrawer(),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _dicesRowWidget(),
            _resultTextWidget(),
            _rollDiceButtonWidget(),
          ],
        ),
      ),
    );
  }

  Widget _dicesRowWidget() {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: _deviceWidth * 0.30,
          height: _deviceHeight * 0.15,
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage(_firstDiceImage),
            ),
          ),
        ),
        Container(
          width: _deviceWidth * 0.30,
          height: _deviceHeight * 0.15,
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage(_secondDiceImage),
            ),
          ),
        ),
      ],
    );
  }

  Widget _rollDiceButtonWidget() {
    return Container(
      width: _deviceWidth,
      margin: EdgeInsets.symmetric(horizontal: _deviceWidth * 0.2),
      decoration: BoxDecoration(
        color: Colors.blue,
        borderRadius: BorderRadius.circular(10),
      ),
      child: MaterialButton(
        onPressed: _generateDiceValue,
        child: const Text("Roll dice!!"),
      ),
    );
  }

  void _generateDiceValue() {
    setState(() {
      _firstDiceValue = random.nextInt(6) + 1;
      _secondDiceValue = random.nextInt(6) + 1;
      _firstDiceImage = 'assets/images/$_firstDiceValue.png';
      _secondDiceImage = 'assets/images/$_secondDiceValue.png';
    });
  }

  Widget _resultTextWidget() {
    int sum = _firstDiceValue + _secondDiceValue;
    return SizedBox(
      width: _deviceWidth,
      child: Text(
        "Total result is: $sum",
        textAlign: TextAlign.center,
        style: const TextStyle(
          fontSize: 25,
        ),
      ),
    );
  }

  Widget _buildDrawer() {
    return Drawer(
      child: ListView(
        children: [
          ListTile(
            title: const Text("Dark theme"),
            trailing: Switch(
              value: _themeProvider.isDarkTheme,
              onChanged: (value) {
                _themeProvider.toggleTheme();
              },
            ),
          ),
        ],
      ),
    );
  }
}
